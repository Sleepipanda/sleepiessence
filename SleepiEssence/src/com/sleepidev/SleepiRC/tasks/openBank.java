package com.sleepidev.SleepiRC.tasks;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.task.Task;
import com.sleepidev.SleepiRC.SleepiRC;

public class openBank extends Task {
	Area BankingArea;
	String[] AllowedItems;
	GameObject Bankbooth;
	public openBank(Area a,String... AIs){
	BankingArea = a;
	AllowedItems = AIs;
	
	}
	@Override
	public void execute() {
		SleepiRC.setStatus("Opening Bank");
		Camera.turnTo(Bankbooth);
		Bankbooth.interact("Bank",Bankbooth.getDefinition().getName());
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		Bankbooth = GameObjects.newQuery().names("Bank booth").results().nearest();
			if(Players.getLocal().getAnimationId() ==-1 && 
					BankingArea.contains(Players.getLocal()) && 
					!Bank.isOpen() && 
					Bankbooth!=null && 
					(Inventory.isEmpty()||Inventory.containsAnyExcept(AllowedItems))){
				return true;
			}
		return false;
		}

}
