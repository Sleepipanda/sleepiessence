package com.sleepidev.SleepiRC.tasks;

import com.runemate.game.api.client.ClientUI;
import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.RuneScape;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.AbstractScript;
import com.runemate.game.api.script.framework.core.EventDispatcher;
import com.runemate.game.api.script.framework.task.Task;
import com.runemate.game.api.script.framework.task.TaskScript;
import com.sleepidev.SleepiRC.SleepiRC;
import com.sun.prism.paint.Stop;

public class withdrawItemsFromBank extends Task{

	private Area BankingArea;
	private String ItemType;
	private String[] ItemNames;
	private boolean isItemRequired;
	private int validated;
	public withdrawItemsFromBank(Area a,boolean Req,String it,String... in){
		BankingArea = a;
		isItemRequired = Req;
		ItemType = it;
		ItemNames = in;
	}
	
	@Override
	public void execute() {
		SleepiRC.setStatus("Withdrawing From Bank");
		for (String item:ItemNames){
			if(Bank.containsAnyOf(item)&&!Inventory.isFull()){
				Bank.withdraw(item, 28);
			}
		}
	}

	@Override
	public boolean validate() {
		if (Players.getLocal().getAnimationId() ==-1 && BankingArea.contains(Players.getLocal()) && Bank.isOpen() &&  Inventory.isEmpty()){
				for (String item:ItemNames){
					if(Bank.containsAnyOf(item)){
						return true;
					}
					System.out.println("Bank does not contain "+item+" checking next item");
				}
				if(isItemRequired){
					System.out.println("Bank did not contain any items of type "+ItemType+" Bot is now shutting down");
					AbstractScript script = Environment.getScript();
					if(script!=null){
						script.stop();
						remove(this);
					}
				}
			return false;
		}
		return false;
	}

}
