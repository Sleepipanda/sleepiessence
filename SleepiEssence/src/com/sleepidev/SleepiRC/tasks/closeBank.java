package com.sleepidev.SleepiRC.tasks;

import java.awt.ItemSelectable;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.entities.Item;
import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.task.Task;
import com.sleepidev.SleepiRC.SleepiRC;

public class closeBank extends Task {

	String[] AllowedItems;
	GameObject Bankbooth;
	public closeBank(String... AIs){
	AllowedItems = AIs;
	}
	
	@Override
	public void execute() {
		SleepiRC.setStatus("Colsing Bank");
		Bank.close();
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		Bankbooth = GameObjects.newQuery().names("Bank booth").results().nearest();
		if(Bank.isOpen() && 
				!Inventory.containsAnyExcept(AllowedItems)&&
				(Inventory.isFull()||!Inventory.containsAnyOf(AllowedItems))){
			return true;
		}
		return false;
	}

}
