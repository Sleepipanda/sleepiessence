package com.sleepidev.SleepiRC.tasks;

import com.runemate.game.api.hybrid.entities.GameObject;
import com.runemate.game.api.hybrid.local.Camera;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.region.GameObjects;
import com.runemate.game.api.script.framework.task.Task;

public class interactGameObject extends Task{
	String Action;
	String Name;
	String[] ReqItems;
	GameObject go;
	
	
	public interactGameObject(String n, String a, String... items){
		Action = a;
		Name = n;
		ReqItems = items;
	}
	@Override
	public void execute() {
		Camera.turnTo(go);
		go.interact(Action,Name);
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean validate() {
		go = GameObjects.newQuery().names(Name).results().nearest();
		if (go!= null){
			if(go.isVisible() &&!Inventory.containsAnyExcept(ReqItems)){
				
				return true;
			}
			 return false;
		}
		// TODO Auto-generated method stub
		return false;
	}

}
