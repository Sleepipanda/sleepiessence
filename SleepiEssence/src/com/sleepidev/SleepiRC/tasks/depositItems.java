package com.sleepidev.SleepiRC.tasks;

import com.runemate.game.api.hybrid.local.hud.interfaces.Bank;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.script.framework.task.Task;

public class depositItems extends Task{

	String[] depositableItems;
	
	public depositItems(String... di){
		depositableItems = di;
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		for (String item:depositableItems){
			if (Inventory.containsAnyExcept(depositableItems)){
				Bank.deposit(item, Inventory.getQuantity(item));
			}
			else 
				Bank.depositInventory();
		}
	}

	@Override
	public boolean validate() {
		if(Bank.isOpen()&&Inventory.containsAnyOf(depositableItems)){
			return true;
		}
		return false;
	}
	

}
