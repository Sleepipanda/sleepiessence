package com.sleepidev.SleepiRC.tasks;

import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.task.Task;

public class walkToBank extends Task {

	Area destination;
	String[] AllowedItems;
	public walkToBank(Area a,String... AIs){
		destination = a;
		AllowedItems = AIs;
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean validate() {
		if(!Players.getLocal().isMoving() && Inventory.containsAnyExcept(AllowedItems) && !destination.contains(Players.getLocal())){
			return true;
		}
		return false;
	}

}
