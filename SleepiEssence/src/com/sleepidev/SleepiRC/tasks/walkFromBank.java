package com.sleepidev.SleepiRC.tasks;

import java.util.Collection;

import com.runemate.game.api.hybrid.Environment;
import com.runemate.game.api.hybrid.entities.Player;
import com.runemate.game.api.hybrid.local.hud.interfaces.Inventory;
import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.hybrid.location.navigation.Path;
import com.runemate.game.api.hybrid.location.navigation.basic.BresenhamPath;
import com.runemate.game.api.hybrid.region.Players;
import com.runemate.game.api.script.framework.task.Task;
import com.sleepidev.SleepiRC.SleepiRC;

public class walkFromBank extends Task {

	String[] AllowedItems;
	Area destination;
	
	public walkFromBank(Area a,String... AIs){
		destination = a;
		AllowedItems = AIs;
	}
	
	@Override
	public void execute() {
		SleepiRC.setStatus("Walking From Bank");
	 Path p = BresenhamPath.buildTo(destination);
	  if(p.getNext()!=null){
		  p.step();
	  }
	  else {
		  System.out.println("Path Cannot be found");
	  }
		
	}

	@Override
	public boolean validate() {
		if(!Players.getLocal().isMoving() && !Inventory.containsAnyExcept(AllowedItems) && !destination.contains(Players.getLocal())){
			return true;
		}
		return false;
	}

}
