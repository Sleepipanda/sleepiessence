package com.sleepidev.SleepiRC;

import com.runemate.game.api.hybrid.location.Area;
import com.runemate.game.api.hybrid.location.Coordinate;
import com.runemate.game.api.script.framework.task.TaskScript;
import com.sleepidev.SleepiRC.tasks.closeBank;
import com.sleepidev.SleepiRC.tasks.depositItems;
import com.sleepidev.SleepiRC.tasks.interactGameObject;
import com.sleepidev.SleepiRC.tasks.openBank;
import com.sleepidev.SleepiRC.tasks.walkFromBank;
import com.sleepidev.SleepiRC.tasks.withdrawItemsFromBank;

public class SleepiRC extends TaskScript{
	static volatile String botStatus;
	String[] essence = {"Rune essence","Pure essence"};
	String[] finishedProducts = {"Mind rune"};
	
	@Override
	public void onStart(String... args){
		setLoopDelay(500,2000);
		Area faladorWestBank = new Area.Rectangular(new Coordinate(2947,3367,0),new Coordinate(2942,3373,0));
		Area mindRuneAlter = new Area.Rectangular(new Coordinate(2978,3510,0),new Coordinate(2983,3514,0));
		
		add(new withdrawItemsFromBank(faladorWestBank,true,"Essence",essence));
		add(new openBank(faladorWestBank,essence));
		add(new closeBank(essence));
		add(new interactGameObject("Altar", "Craft-rune",essence));
		add(new interactGameObject("Mysterious ruins", "Enter",essence));
		add(new interactGameObject("Portal", "Use", finishedProducts));
		add(new walkFromBank(faladorWestBank ,finishedProducts));
		add(new walkFromBank(mindRuneAlter ,essence));
		add(new depositItems(finishedProducts));

		//add(new interactGameObject("Mysterious ruins", "Enter"));
		//add(new interactGameObject("Mysterious ruins", "Enter"));
		
	}
	
	public static void setStatus(String newStatus){
		System.out.println(newStatus);
	}
	

}
