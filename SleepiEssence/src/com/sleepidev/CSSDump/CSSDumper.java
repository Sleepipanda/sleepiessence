package com.sleepidev.CSSDump;

import com.runemate.game.api.client.ClientUI;
import com.runemate.game.api.client.paint.PaintListener;
import com.runemate.game.api.hybrid.util.Resources;
import com.runemate.game.api.script.framework.LoopingScript;
import com.sun.javafx.application.PlatformImpl;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionModel;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import jdk.internal.dynalink.support.ClassMap;

import java.awt.*;
import java.io.IOException;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
 
public final class CSSDumper extends LoopingScript implements PaintListener {
	 Stage stage;
	 Scene scene;
	 double initialX;
	 double initialY;
	@Override
    public void onStart(String... args) {
	
	}
 
    @Override
    public void onLoop() {
        //logic goes here
    }
 
    @Override
    public void onPaint(Graphics2D g2d) {
    }
    
    @Override
    public void onStop(){
    	stage.close();
    }
}
